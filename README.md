# articlewidth
### _App shows an example of using interfaces with structs_

#### To run:
```
cd /src
go build
./articlewidth
```
or
```
go run src/articlewidth.go  
```

#### Example test:
```
http://localhost:3000/?length=10000&width=100
```
----

#### Avaliable parameters:
__width__ – article column width (an integer number from 0 to 500)  
__length__ – number of words in article (an integer number from 0 to 100,000)