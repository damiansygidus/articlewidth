package main

import (
	"bytes"
	"fmt"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"
)

var words = [...]string{
	"lore", "lipsum", "dolor", "sitamet", "consectetur", "adipiscing", "elit", "sed", "do",
	"eiusmod", "tempor", "incididunt", "labore", "et", "dolore", "magna", "aliqua", "ut",
	"enim", "ad", "minim", "veniam", "quis", "nostrud", "exercitation", "ullamco", "laboris",
	"nisi", "ut", "aliquip", "ex", "ea", "commodo", "consequat", "duis", "aute", "irure",
	"dolor", "in", "reprehenderit", "in", "voluptate", "velit", "esse", "cillum", "dolore",
	"eu", "fugiat", "nulla", "pariatur", "excepteur", "sint", "occaecat", "cupidatat", "non",
	"proident", "sunt", "in", "culpa", "qui", "officia", "deserunt", "mollit", "anim", "id",
	"est", "laborum",
}

var maxLength int = 100000
var maxWidth int = 500

var ad string = "Sourcecode: https://bitbucket.org/damiansygidus/articlewidth"
var how string = "Avaliable parameters: length={0–" +
	strconv.Itoa(maxLength) + "}, width={0–" +
	strconv.Itoa(maxWidth) + "}"

func main() {
	http.HandleFunc("/", handler)
	http.ListenAndServe(":3000", nil)
}

func getParams(val *url.Values) (int, int, error) {
	length, err := strconv.Atoi(val.Get("length"))
	if err != nil {
		return 0, 0, NewErrWrongInput("length input must be a natural number")
	} else if length < 1 || length > maxLength {
		return 0, 0, NewErrWrongInput("length input must be a number between 1 and 100000")
	}

	width, err := strconv.Atoi(val.Get("width"))
	if err != nil {
		return 0, 0, NewErrWrongInput("width input must be a natural number")
	} else if width < 1 || width > maxWidth {
		return 0, 0, NewErrWrongInput("width input must be a number between 1 and 100000")
	}

	return length, width, nil
}

func handler(w http.ResponseWriter, r *http.Request) {
	printInitMsg(&w)

	q := r.URL.Query()

	length, width, err := getParams(&q)
	if err != nil {
		fmt.Fprintln(w, err.Error())
		return
	}

	a, err := generateArticle(length)
	if err != nil {
		fmt.Fprintln(w, err.Error())
		return
	}

	var wc WriterCloser = NewBufferedWriterCloser()
	wc.Write(w, a, width)
	wc.Close(w, width)
}

func printInitMsg(w *http.ResponseWriter) {
	fmt.Fprintln(*w, ad+"\n"+how+"\n")
}

func generateArticle(articleLength int) ([]byte, error) {
	article := ""
	startArticle(&article, articleLength)
	endArticle(&article)

	if len(article) <= 0 {
		return nil, NewErrWrongInput("Can not generate article")
	}

	return []byte(article), nil
}

func startArticle(a *string, l int) {
	rand.Seed(time.Now().Unix())

	*a = "Lorem ipsum"

	for i := 0; i < l; i++ {
		n := rand.Int() % len(words)
		*a = *a + " " + words[n]

		m := rand.Int() % 100
		if i < l-1 && m > 90 {
			*a = *a + ","
		} else if i < l-1 && m > 20 && m < 25 {
			k := rand.Int() % len(words)
			*a = *a + ". " + strings.Title(words[k])
		}
	}
}

func endArticle(a *string) {
	*a = *a + "."
}

type ErrWrongInput struct {
	message string
}

func NewErrWrongInput(message string) *ErrWrongInput {
	return &ErrWrongInput{
		message: message,
	}
}

func (e *ErrWrongInput) Error() string {
	return e.message
}

type Writer interface {
	Write(http.ResponseWriter, []byte, int) (int, error)
}

type Closer interface {
	Close(http.ResponseWriter, int) error
}

type WriterCloser interface {
	Writer
	Closer
}

type BufferedWriterCloser struct {
	buffer *bytes.Buffer
}

func NewBufferedWriterCloser() *BufferedWriterCloser {
	return &BufferedWriterCloser{
		buffer: bytes.NewBuffer([]byte{}),
	}
}

func (bwc *BufferedWriterCloser) Write(w http.ResponseWriter, data []byte, width int) (int, error) {
	n, err := bwc.buffer.Write(data)
	if err != nil {
		return 0, err
	}

	v := make([]byte, width)
	for bwc.buffer.Len() > width {
		_, err := bwc.buffer.Read(v)
		if err != nil {
			return 0, err
		}
		_, err = fmt.Fprintln(w, string(v))
		if err != nil {
			return 0, err
		}
	}

	return n, nil
}

func (bwc *BufferedWriterCloser) Close(w http.ResponseWriter, width int) error {
	for bwc.buffer.Len() > 0 {
		data := bwc.buffer.Next(width)
		_, err := fmt.Fprintln(w, string(data))
		if err != nil {
			return err
		}
	}

	return nil
}
